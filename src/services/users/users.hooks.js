const { authenticate } = require('@feathersjs/authentication').hooks;

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;
const checkPermissions = require('feathers-permissions');
module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt'), checkPermissions({
      roles:['admin'],
      field: 'role'
    }) ],
    get: [ authenticate('jwt') ],
    create: [ hashPassword('password') ],
    update: [ hashPassword('password'),  authenticate('jwt'), checkPermissions({
      roles:['admin'],
      field: 'role'
    }) ],
    patch: [ hashPassword('password'),  authenticate('jwt'), checkPermissions({
      roles:['admin'],
      field: 'role'
    }) ],
    remove: [ authenticate('jwt'), checkPermissions({
      roles:['admin'],
      field: 'role'
    }) ]
  },

  after: {
    all: [ 
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
