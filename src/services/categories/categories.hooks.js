const { authenticate } = require('@feathersjs/authentication').hooks;
const checkPermissions = require('feathers-permissions');
module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [checkPermissions({
      roles:['admin'],
      field: 'role'
    })],
    update: [checkPermissions({
      roles:['admin'],
      field: 'role'
    })],
    patch: [checkPermissions({
      roles:['admin'],
      field: 'role'
    })],
    remove: [checkPermissions({
      roles:['admin'],
      field: 'role'
    })]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
