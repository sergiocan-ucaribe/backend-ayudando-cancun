const users = require('./users/users.service.js');
const addresses = require('./addresses/addresses.service.js');
const categories = require('./categories/categories.service.js');
const products = require('./products/products.service.js');
const uploads = require('./uploads/uploads.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(addresses);
  app.configure(categories);
  app.configure(products);

  app.configure(uploads);
};
