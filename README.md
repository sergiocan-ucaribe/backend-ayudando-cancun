# backend-ayudando-cancun

> 

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/backend-ayudando-cancun
    npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Example request

* ### `POST`
    Auth user
    * `/authentication`: 
        ```json
        { 
        	"strategy": "local", 
         	"email": "hello@feathersjs.com", 
         	"password": "supersecret" 
        }
        ```
    Create user
    * `/users`: 
        ```json
        { 
         	"email": "hello@feathersjs.com", 
            "password": "supersecret",
            "firstName": "Feather",
            "lastName": "Api",
            "phoneNumber":"9981234567",
        }
        ```
    Create address
    * `/addresses`:
        ```json
        {
        	"region": {
              "latitude": 37.78825,
              "longitude": -122.4324,
              "latitudeDelta": 0.0922,
              "longitudeDelta": 0.0421
            },
        	"user": "5ea8838f50347e3b07e17128"
        }
        ```
    Upload image
    * `/uploads`:
        ```json
        {
              "uri":"image"
        }
        ```

* ### `GET`

    * `/users`: Must send authorization header from `authentication` 
    * `/users/:idUser`: Must send authorization header from `authentication`
    * `/addresses/?user=:idUser`: Must send authorization header from `authentication` and query params:
        ```json
            {
                "user": "5ea8838f50347e3b07e17128"
            }
        ```
    
* ### `PUT`
    * `/users/:idUser`: Must send authorization header from `authentication` and values to update
        ```json
            { 
             	"email": "hello2@feathersjs.com", 
            }
        ```
* ### `DELETE`
    * `/users/:idUser`: Must send authorization header

